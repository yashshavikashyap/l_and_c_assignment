public class Wallet {
    private double value; 
    public Wallet(){
        this.value = 0;
    }
    public double getTotalMoney() { 
        return value; 
    }
    public void setTotalMoney(double newValue) {
        value = newValue; 
    } 
 
    public void addMoney(double deposit) {
        value += deposit; 
    } 
 
    public void subtractMoney(double debit) {
        value -= debit; 
    } 
} 
