"""Send and Fetch tumblr api response.
This module send request using tumblr api with
the user defined parameters, blog name, starting post number
and ending postnumber to fetch for a blog.
"""
import requests

def fetch_tumblr_api_response(blog_name, start_postno, end_postno):
    """method fetch response using request library
       using the parameters for starting offset,and number of posts,
       Returns
        fetched response
    """
    url = "https://%s.tumblr.com/api/read/json" % (blog_name)
    #get the number of posts required rom starting offset given by user using start and end post no
    posts_count = (end_postno - start_postno) + 1
    params = {'type' : 'photo',
              'num' : posts_count,
              'start': start_postno - 1
             }
    response = requests.get(url, params)
    return response
