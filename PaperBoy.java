/* ************************************************* Answer ****************************************************** */

/* Although wallet object is private to customer class yet we are providing full access of it to paperboy class through getWallet() method which is 
breaching the security of wallet class. In the above approach, paperboy class has all the information of wallet class which is not required and is 
security-critical. Also through getWallet() method, the paperboy class will have access to all functionalities that can be added to the wallet class in the future. 
While to ensure the security of wallet class, it's object should remains private to customer class only.
*/

// Code - 
// Paperboy class

public class PaperBoy {
    public static void main(String[] args){
        double payment = 2.00; // I want my two dollars!
        Customer myCustomer = new Customer();
        myCustomer.doPayment(payment); 
    }
}
    
/* 
Now all the implementation related to wallet class are done inside customer class only and "how 
the payment is done?" is hidden from the paperboy class, only whether the payment is done or not is known to 
paperboy class which was desired. Using this implementation paperboy knows things that are required, all other 
functionalities of wallet class is abstracted from the paperboy class. 
*/

/* ************************************************************************************************************ */