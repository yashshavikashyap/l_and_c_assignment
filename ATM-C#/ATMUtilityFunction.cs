﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace ATM_C_
{
    ///<summary>Contains Helper functions for ATM process</summary>
    public class Utility
    {
        public BlockedATMCard blockedCards;
        private double AmountInATM = 10000;

        public Utility()
        {
            blockedCards = new BlockedATMCard();
        }

        public double GetAvailableBalanceInATM()
        {
            return AmountInATM;
        }

        public void CheckForSpecialCharacters(string cardNumber)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            if (!regexItem.IsMatch(cardNumber))
            {
                throw new InputCardNumberHasSpecialChar("Inavlid Card!!");
            }
        }

        public void CheckForEmptyString(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber))
            {
                throw new InputCardNumberIsEmptyString("Inavlid Card!!");
            }
        }

        public void CheckForLength(string cardNumber)
        {
            if (cardNumber.Length != 16)
            {
                throw new InputCardNumberExceedLength("Inavlid Card!!");
            }
        }

        public void CheckCardExists(string cardNumber)
        {
            string data = File.ReadAllText(@"cardDetails.txt");
            var cardDetails = JObject.Parse(data);
            if (cardDetails[cardNumber] == null)
            {
                throw new CardNotExistsException("Card doesn't exists");
            }
        }

        public void IsCardBlocked(string inputCard)
        {
            int cardBlockStatus = File.ReadAllText(@"BlockCards.txt").Contains(inputCard) ? 1 : 0;
            if (cardBlockStatus == 1)
            {
                throw new CardIsBlockedException("Your card is blocked!!!");
            }
        }

        public void  VerifyCardNumber(string cardNumber)
        {
            try
            {
                CheckForEmptyString(cardNumber);
                CheckForLength(cardNumber);
                CheckForSpecialCharacters(cardNumber);
                CheckCardExists(cardNumber);
                IsCardBlocked(cardNumber);
            }
            catch (InvalidCardException ex)
            {
                Console.WriteLine(ex.Message);
                ExitFromProcess();
            }
        }

        public JToken GetCardDetails(string cardNumber)
        {
            string data = File.ReadAllText(@"cardDetails.txt");
            var cardDetails = JObject.Parse(data);
            return cardDetails[cardNumber];
        }

        public void CheckForThreeAttempt(JToken card, int attempt)
        {
            if (attempt >= 3)
            {
                Console.WriteLine(card.Parent.Path); //parent.Properties().Select(p => p.Name));
                this.blockedCards.AddToBlockCard(card.Parent.Path);
                throw new TooManyInvalidCrediantialAttemptException("Too many invalid crediantial attempts...Card Blocked.. exiting");
            }
        }

        public void VerifyPinCode(JToken card, string pincode)
        {
            if (((string)card["pin"]) != pincode)
            {
                throw new InvalidPinException("Invalid Crediential Exception");
            }
        }

        public BankAccount ExitFromProcess()
        {
            Console.WriteLine("Thank you for service.\n Exiting from process........");
            ATMMachine machine = new ATMMachine();
            return machine.ReadInput();
        }
    }
}
