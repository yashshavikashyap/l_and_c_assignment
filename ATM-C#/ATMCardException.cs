﻿using System;
///<summary> Exceptions occured due to ATM Card or Machine</summary>
public class InvalidCardException : Exception
{
    public InvalidCardException(string message) : base(message)
    {
    }
}

public class CardNotExistsException : InvalidCardException
{
    public CardNotExistsException(string message) : base(message)
    {
    }
}

public class CardIsBlockedException : InvalidCardException
{
    public CardIsBlockedException(string message) : base(message)
    {
    }
}

public class InputCardNumberHasSpecialChar : InvalidCardException
{
    public InputCardNumberHasSpecialChar(string message) : base(message)
    {
    }
}

public class InputCardNumberIsEmptyString : InvalidCardException
{
    public InputCardNumberIsEmptyString(string message) : base(message)
    {
    }
}

public class InputCardNumberExceedLength : InvalidCardException
{
    public InputCardNumberExceedLength(string message) : base(message)
    {
    }
}
