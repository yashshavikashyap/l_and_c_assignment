﻿using System;
///<summary> Exceptions occured due to ATM Card or Machine</summary>

public class InvalidCredientialException : Exception
{
    public InvalidCredientialException(string message) : base(message)
    {
    }
}

public class InvalidPinException : InvalidCredientialException
{
    public InvalidPinException(string message) : base(message)
    {
    }
}

public class TooManyInvalidCrediantialAttemptException : Exception
{
    public TooManyInvalidCrediantialAttemptException(string message) : base(message)
    {
    }
}

public class InsufficientBalanceInATMException : Exception
{
    public InsufficientBalanceInATMException(string message) : base(message)
    {
    }
}
