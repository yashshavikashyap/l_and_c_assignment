﻿using System;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace ATM_C_
{
    class ATMMachine
    {
        public static Utility utils;
        public static BankAccount cardSelected;

        public static Client client;

        public ATMMachine()
        {
            utils = new Utility();
            client = new Client();
        }

        public void VerifyCredientialAndCheckForAttemptCount(JToken card, string pincode, int attempt)
        {
            try
            {
                utils.VerifyPinCode(card, pincode);
            }
            catch (InvalidPinException ex)
            {
                Console.WriteLine(ex.Message);
                attempt += 1;
                utils.CheckForThreeAttempt(card, attempt);
                ReadPinCode(card, attempt++);
            }
        }

        public void ReadPinCode(JToken cardDetails, int attempt)
        {
            Console.WriteLine("Enter PIN Code");
            string pinCode = Console.ReadLine();
            VerifyCredientialAndCheckForAttemptCount(cardDetails, pinCode, attempt);
        }

        public void IsEnoughCashAvailableInATM(double amount)
        {
            if (amount > utils.GetAvailableBalanceInATM())
            {
                throw new InsufficientBalanceInATMException("ATM DOESN'T HAVE THAT MUCH BALANCE");
            }
        }

        public BankAccount ReadInput()
        {
            Console.WriteLine("\n\n-----------------Welcome To The ATM---------------------");
            Console.WriteLine("Enter card number");
            string cardNumber = Console.ReadLine();
            utils.VerifyCardNumber(cardNumber);
            JToken cardDetail = utils.GetCardDetails(cardNumber);

            try
            {
                ReadPinCode(cardDetail, 0);
            }
            catch (TooManyInvalidCrediantialAttemptException ex)
            {
                Console.WriteLine(ex.Message);
                return utils.ExitFromProcess();
            }
            return SelectCard(cardDetail);
        }

        public BankAccount SelectCard(JToken card)
        {
            return new BankAccount(card);
        }

        public static void Main(string[] args)
        {
            ATMMachine atm = new ATMMachine();
            bool process = true;
            client.ExecuteClient();
            cardSelected = atm.ReadInput();
            double amount;
            while (process)
            {
                Console.WriteLine("\n*****Choose operation*****");
                Console.WriteLine(" 1. Deposit");
                Console.WriteLine(" 2. Withdraw");
                Console.WriteLine(" 3. Balance Enquiry");
                Console.WriteLine(" 4. Exit");
                Char option = (char)Console.Read();
                Console.ReadLine();//TO CONSUME INPUT STREAM
                try
                {
                    switch (option)
                    {
                        case '1':
                            {
                                Console.WriteLine("Enter amount to Deposit: ");
                                amount = Convert.ToDouble(Console.ReadLine());
                                cardSelected.Deposit(amount);
                                Console.WriteLine("Amount " + amount + "Deposited");
                                break;
                            }
                        case '2':
                            {
                                Console.WriteLine("Enter amount to Withdraw: ");
                                amount = Convert.ToDouble(Console.ReadLine());
                                atm.IsEnoughCashAvailableInATM(amount);
                                double withdrawalAmount = cardSelected.Withdraw(amount);
                                Console.WriteLine("Collect " + withdrawalAmount + " cash");
                                break;
                            }
                        case '3':
                            {
                                Console.WriteLine("Current Balance: " + cardSelected.CheckBalance());
                                break;
                            }
                        // exit operation 
                        case '4':
                            {
                                cardSelected = utils.ExitFromProcess();
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Invalid option");
                                break;
                            }
                    }
                }
                catch (InvalidAmountException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (InsufficientBalanceException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (OverflowException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (InsufficientBalanceInATMException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
