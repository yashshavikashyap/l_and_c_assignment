﻿// BookInterface is interface for class book
// Operation class is contains Main function
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace ATM_C_
{
    ///<summary>Methods need to be implement by ATM Card</summary>
    interface ATMCardInterface  //CREATING INTERFACE FOR CLASS BOOK
    {
        public double Withdraw(double amount); // method for Withdrawing money
        public void Deposit(double amount);     // method for adding money to account
        public double CheckBalance();   //method for check balance
    }
}