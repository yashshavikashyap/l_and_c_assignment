﻿using System;
public class InvalidAmountException : Exception
{
    public InvalidAmountException(string message) : base(message)
    {
    }
}

public class InsufficientBalanceException : Exception
{
    public InsufficientBalanceException(string message) : base(message)
    {
    }
}

public class NotAbleToConnectToServer : Exception
{
    public NotAbleToConnectToServer(String message) : base(message)
    {
    }
}
