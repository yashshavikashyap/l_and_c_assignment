﻿using System;
using Newtonsoft.Json.Linq;
using System.IO;
namespace ATM_C_
{
    public class BankAccount : ATMCardInterface
    {
        private double accountBalance;

        public BankAccount()
        {
            this.accountBalance = 0;
        }

        public BankAccount(JToken card)
        {
            this.accountBalance = GetBaseBalance(card); // initialise account with base balance 
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new InvalidAmountException("Invalid amount; amount<=0");
            }

            this.accountBalance += amount;
        }

        public double Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new InvalidAmountException("Invalid amount; amount<=0");
            }
            if (amount > accountBalance)
            {
                throw new InsufficientBalanceException("YOUR ACCOUNT DOESN'T HAVE ENOUGH BALANCE");
            }
            this.accountBalance -= amount;
            return amount;
        }

        public double CheckBalance()
        {
            return this.accountBalance;
        }

        public static double GetBaseBalance(JToken card){
            return (double)card["baseBalance"];
        }
    }

    // public class BankCard1 : BankAccount
    // {
    //     public BankCard1() : base(GetBaseBalance(0)) {
            
    //      }
    // }

    // public class BankCard2 : BankAccount
    // {
    //     public BankCard2() : base(GetBaseBalance(1))
    //     {

    //     }
    // }

    // public class BankCard3 : BankAccount
    // {
    //     public BankCard3() : base(GetBaseBalance(2))
    //     {

    //     }
    // }
}
