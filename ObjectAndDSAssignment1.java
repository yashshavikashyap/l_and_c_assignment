/* ******************************************** Question ************************************************************* */

Class Employee { 
  string name;
  int age; 
  float salary; 
  public : string getName(); 
  void setName(string name); 
  int getAge(); 
  void setAge(int age); 
  float getSalary();
  void setSalary(float salary);
}; 
Employee employee; 

// Is 'employee' an object or a data structure? Why ?


/* *********************************************** Answer ************************************************************** */

/*
"employee" is a data structure as it's class "Employee" is exposing data and have no significant behavior associated
with it. Class "Employee" is exposing the data through getter and setter methods while objects
have some functions which expose some behavioral characteristics and hide the data. 

For example -
If the class employee is like following then it would be object. 
*/

Class Employee { 
  string name;
  int age; 
  float salary; 

  public void doWork(string name, int age){
      // describes behaviour
  }
}; 
Employee employee; 

/* 
Now a behavior is added to the class employee and hence it will be an object as doWork() is now hiding the data
but exposing the behavior of object "employee".
*/

