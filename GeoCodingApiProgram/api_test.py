"""
learning test cases for api
"""
import unittest   # The test framework
import configparser
from ddt import ddt, data, unpack
from api_test_helper import TestHelper

@ddt
class TestOpencageApi(unittest.TestCase):
    """ 
    Intialise test helper class object to have api key and request api url 
    """
    def setUp(self):
        self.test_helper = TestHelper()
        
    """
    Api requests given latitude and longitude in result,
    geometey component, inside object lng and lat respectively 
    """
    @data(("Madrid", [-3.7035825, 40.4167047]))
    @unpack
    def test_getLongitudeAndLatitude_pass(self, input_place_name, expected_lang_lat):
        response = self.test_helper.fetch_response_from_geocoding_api(input_place_name)
        result = response['results']
        longitude = result[0]['geometry']['lng']
        latitude  = result[0]['geometry']['lat']
        self.assertEqual(longitude, expected_lang_lat[0], "Fetched langitude is wrong")
        self.assertEqual(latitude, expected_lang_lat[1], "Fetched latitude is wrong")
    
    """ 
    Api shows status code 200 for string of lengths more than unit length 
    """
    @data("Madrid", "Jaipur", "Rajasthan", "Newyork", "$$", "kji", "*()", "__")
    def test_getLongitudeAndLatitude_successRequestStatusCode_pass(self, input_place_name):
        response = self.test_helper.fetch_response_from_geocoding_api(input_place_name)
        response_status = response['status']
        self.assertEqual(response_status['code'], 200, "Status code is not 200")

    """ 
    Api give empty result body for invalid name
    """
    @data("$%", ".;;", "c", "j", "", " ", "silboard")
    def test_getLongitudeAndLatitude_emptyResponse(self, input_place_name):
        response = self.test_helper.fetch_response_from_geocoding_api(input_place_name)
        result = response['results']
        self.assertEqual(result, [], "Empty response is not returned")
    
    """
    Api requests shows bad request 400 status only, if operators value are given or unit length char input is given
    """
    @data("$", ".", "c", "j", "", " ")
    def test_getLongitudeAndLatitude_badRequestStatusCode_400(self, input_place_name):
        response = self.test_helper.fetch_response_from_geocoding_api(input_place_name)
        response_status = response['status']
        self.assertEqual(response_status['code'], 400, "Status code is not 400")

if __name__ == '__main__':
    unittest.main()
    