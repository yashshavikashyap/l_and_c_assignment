import requests
import configparser
"""
test helper class for api_tests
"""
class TestHelper:
    """ 
    Set url and paramaters for requests
    """
    def __init__(self):
        intial_url, api_key = self.get_api_key_and_request_url()
        self.url = intial_url
        self.params = {'pretty' : 1,
                       'key': api_key
                      }
    
    """ 
    Get api key and request api url from configuration file
    """
    def get_api_key_and_request_url(self):
        config = configparser.ConfigParser()
        config.read('configuration.ini')
        return config['GeoCodingApi']['Url'], config['GeoCodingApi']['ApiKey']

    """ 
    Fetch response from api
    """
    def fetch_response_from_geocoding_api(self, input_place_name):
        self.params['q'] = input_place_name
        raw_response = requests.get(self.url, params= self.params)
        return raw_response.json()
