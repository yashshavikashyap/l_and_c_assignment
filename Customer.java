class InSufficientBalanceException extends Exception 
{ 
    public InSufficientBalanceException(String s) 
    { 
       super (s) ;
    } 
} 
public class Customer { 
    private String firstName; 
    private String lastName;
    private Wallet myWallet; 
    public Customer(){
        this.firstName = "Yashshavi";
        this.lastName = " Kashyap";
        this.myWallet = new Wallet();
    }   
    public String getFirstName(){
         return firstName; 
        } 

    public String getLastName(){ 
        return lastName; 
        }

    public void doPayment(double payment){
        double totalMoney = this.myWallet.getTotalMoney();
        String msgForNotEnoughMoney = "Not have enough money for payment";
        String exceptionCaughtMsg = "Caught the exception";
        String msgForPaperBoy = "come back later and get my money";
        try
        {
           if(totalMoney < payment)
            {
                throw new InSufficientBalanceException(msgForNotEnoughMoney);
            }
            else
            {
                myWallet.subtractMoney(payment);
                System.out.println("payment done");
            }
        }
        catch(InSufficientBalanceException ex)
        {
            System.out.println(exceptionCaughtMsg);
            System.out.println(ex.getMessage());
            System.out.println(msgForPaperBoy);
        }
    }
}
