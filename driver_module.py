"""Main Driver program.
This module contains main driver function and functions for displaying
various outputs of the api responses. The functions are main function
which invokes various functions to complete task of fetching tumblr api response
with desired output i.e basic information of user defined input blog
and the highest quality photos url. The functions to display basic information
recieved from the tumblr api response and the function to display list of  url containing photos of
highest quality defination for asked post range are also in this module.
"""
import utils as ut
import api_response as api

NONEXIST_BLOG_NAME_ERROR_MSG = 'Enter valid blog name'

def display_basic_info(json_data):
    """display title, name, description and no of posts from fetched json data of blog"""
    print('title: ', json_data['tumblelog']['title'])
    print('\ndescription: ', json_data['tumblelog']['description'])
    print('\nname: ', json_data['tumblelog']['name'])
    print('\nno of post: ', json_data['posts-total'])

def display_list_of_url(json_data, start_postno, end_postno):
    """display list of url containing photos of highest quality defination for asked post range"""
    print("\nPhoto url - \n")
    post_no = start_postno
    completed = False
    while not completed:
        posts = json_data['posts']
        post_no = ut.print_photo_urls(posts, post_no)
        # for more then 50 api post requests, break loop
        # if total number of posts of user are covered
        # incase user has asked for posts number greater then blog's total post
        # or if all the posts in the asked post range are covered
        if (post_no) == (end_postno+1) or post_no >= json_data['posts-total']:
            completed = True
        else:
            json_data = get_tumbri_api_json_response(json_data['tumblelog']['name']
                                                     , post_no, end_postno)

def get_tumbri_api_json_response(blog_name, start_postno, end_postno):
    """ fetch response from tumblr api, do data processing and return parsed data in json format """
    response = api.fetch_tumblr_api_response(blog_name, start_postno, end_postno)
    required_data = ut.remove_unnecessary_data(response)
    json_data = ut.parse_into_json(required_data)
    return json_data

def main():
    """driver class, call all funtion to perform required task"""
    blog_name, post_range = ut.take_input()
    start_postno, end_postno = ut.get_start_and_end_postno(post_range)
    json_data = get_tumbri_api_json_response(blog_name, start_postno, end_postno)
    if json_data['tumblelog']['name']:
        display_basic_info(json_data)
        display_list_of_url(json_data, start_postno, end_postno)
    else:
        print(NONEXIST_BLOG_NAME_ERROR_MSG)

#call main driver function to perfom tasks.
main()
