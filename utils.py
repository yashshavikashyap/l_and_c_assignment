"""utility functions
This modules contains helper functions.
It contains taking user input utility function,
input validity checking function,
getting start and end post number from given post range,
removing unneccesary data from recieved tumblr api response,
parsing recieved tumblr api reponse into json format, filtering
and printing photo urls from the recieved api response.
"""
import sys
import json
import re

BLOG_NAME_QUERY_MSG = "Enter blog name "
POST_RANGE_QUERY_MSG = "Enter post range for which you want data "
INVALID_INPUT_ERROR_MSG = "Please enter valid post-range "
HIGH_DEFINATION_IMG_QUALITY = 'photo-url-1280'

def take_input():
    """method extracts input from user for blog_name and post_range
       Returns
        (blog_name, post_range)
    """
    blog_name = input(BLOG_NAME_QUERY_MSG)
    post_range = input(POST_RANGE_QUERY_MSG)
    return (blog_name, post_range)

def check_validity_of_input_range(start_postno, end_postno):
    """ method check whether the input range is valid, non negetive numbers
        and starting post no is less than ending post no
    """
    status = True
    if not (start_postno.isdigit() and end_postno.isdigit()):
        status = False
    elif int(start_postno) <= 0 or int(end_postno) <= 0:
        status = False
    elif int(start_postno) > int(end_postno):
        status = False
    if not status:
        print(INVALID_INPUT_ERROR_MSG)
        sys.exit()

def get_start_and_end_postno(post_range):
    """method extracts starting and ending post number from given post range
       Returns
        (starting post no , end postno)
    """
    regex_seprator = [x.end() for x in re.finditer(r'[0-9]+-', post_range)]
    if regex_seprator:
        splitting_index = int(regex_seprator[0]) - 1
        start_postno = post_range[0:splitting_index]
        end_postno = post_range[splitting_index + 1:]
        check_validity_of_input_range(start_postno, end_postno)
        return (int(start_postno), int(end_postno))
    else:
        print(INVALID_INPUT_ERROR_MSG)
        sys.exit()

def remove_unnecessary_data(response):
    """clean data and making compatible for parsing into json
       Return
        required data for parsing into json
    """
    fetched_data = response.text
    required_data = fetched_data.replace("var tumblr_api_read = ", "")[:-2]
    return required_data

def parse_into_json(data):
    """parse data into json
       Return
        json data
    """
    json_data = json.loads(data)
    return json_data

def print_photo_urls(posts, post_no):
    """print photo urls of highest quality photos from posts
       Returns
        Last post number displayed
    """
    posts_count = len(posts)
    for i in range(0, posts_count):
        photo_url = posts[i][HIGH_DEFINATION_IMG_QUALITY]
        if posts[i][HIGH_DEFINATION_IMG_QUALITY]:
            print(post_no)
            #check if multiple photos
            if posts[i]['photos']:
                photos = posts[i]['photos']
                for photo in photos:
                    print(photo[HIGH_DEFINATION_IMG_QUALITY])
            else:
                print(photo_url)
        post_no += 1
    return post_no
