"""
Exception classes for geocoding api
"""
class GeoCodingApiException(Exception): 
    def __init__(self, msg): 
        self.msg = msg 
    
class InvalidAddressException(GeoCodingApiException): 
    def __init__(self, msg): 
        self.msg = msg 
    
class EmptyResponseException(GeoCodingApiException):
    def __init__(self, msg): 
        self.msg = msg 
    
class BadRequestException(GeoCodingApiException):
    def __init__(self, msg): 
        self.msg = msg 

class UnAuthorisedException(GeoCodingApiException):
    def __init__(self, msg): 
        self.msg = msg 
    
class RateLimitExceededException(GeoCodingApiException):
    def __init__(self, msg): 
        self.msg = msg 
    
class TimeoutException(GeoCodingApiException):
    def __init__(self, msg): 
        self.msg = msg 
    
class RequestTooLongException(GeoCodingApiException):
    def __init__(self, msg): 
        self.msg = msg 
    
class InternalServerException(GeoCodingApiException):
    def __init__(self, msg): 
        self.msg = msg 
    
