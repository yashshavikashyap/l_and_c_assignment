""" 
Driver application, which contains user program
"""
import configparser
from geocoding_api import GeoCodingApi
from utils import Utility
from geocoding_api_exceptions import GeoCodingApiException

class GeoCodingConsoleApplication(GeoCodingApi):            
        
    invalid_input_msg = "\nplease enter valid credientials\n"
    input_msg  = "\nplease enter a place name \n"
    user_choice_input_msg = "\npress 1 to continue \n"
    
    def __init__(self): 
        GeoCodingApi.__init__(self)
        self.utils = Utility()
    
    """
    Take input address
    """
    def take_input_address(self):
        address = input(self.input_msg)
        self.utils.validate_input_string(address)
        return address

    """ 
    Display longitude and latitude on output sceen
    """
    def display_longitude_and_latitude(self, longitude, latitude):
        print("Longitude " , end='')
        print(longitude)
        print("Latitude ", end='')
        print(latitude)

    """
    Ask user for choice if he wants to continue  
    """
    def ask_user_for_continue(self):
        choice = input(self.user_choice_input_msg)
        if choice == '1':
            return True
        else :
            return False

    """ 
    Driver module
    """
    def main(self):
        geocoding_object = GeoCodingConsoleApplication() 
        user_choice = True
        while user_choice:
            try:
                address = geocoding_object.take_input_address()
                longitude, latitude = geocoding_object.get_longitude_and_latitude(address)
                geocoding_object.display_longitude_and_latitude(longitude, latitude)
            except GeoCodingApiException as error:
                print(error.msg)
                continue
            finally:
                user_choice = geocoding_object.ask_user_for_continue()

if __name__ == "__main__":
    GeoCodingConsoleApplication().main()
