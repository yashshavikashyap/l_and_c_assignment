"""
Wrapper class for geocoding api service
"""
import requests
import configparser
from utils import Utility
from geocoding_api_exceptions import EmptyResponseException

class GeoCodingApi():
    empty_Response_Error_Msg = "\nSorry, could not find the latitude and longitude for given place\n"
    output_msg = "\nShowing latitude and longitude for the location %s\n"

    def __init__(self): 
        intial_url, api_key = self.get_api_key_and_request_url()
        self.url = intial_url
        self.params = {'pretty' : 1,
                       'key': api_key
                      }
        self.utils = Utility()

    """ 
    Get api key and request api url from configuration file
    """
    def get_api_key_and_request_url(self):
        config = configparser.ConfigParser()
        config.read('configuration.ini')
        return config['GeoCodingApi']['Url'], config['GeoCodingApi']['ApiKey']

    """ 
    fetch geocoding api response
    """
    def fetch_geocoding_api_response(self, input_place_name):
        self.params['q'] = input_place_name
        raw_response = requests.get(self.url, params= self.params)
        response = raw_response.json()
        self.utils.check_status_code(response['status'])
        return response
    
    """ 
    fetch longitude and latitude from the response returned by the api
    """
    def get_longitude_and_latitude(self, input_place_name):
        response = self.fetch_geocoding_api_response(input_place_name)
        result = response['results']
        try:
            longitude = result[0]['geometry']['lng']
            latitude = result[0]['geometry']['lat']
            location = result[0]['formatted']
            print(self.output_msg % location)
            return longitude, latitude
        except IndexError:
            raise(EmptyResponseException(self.empty_Response_Error_Msg))
