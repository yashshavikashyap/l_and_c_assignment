"""
utility class provide helper functions for 
geocoding_api_console application
"""
import re
from geocoding_api_exceptions import InvalidAddressException
from geocoding_api_exceptions import EmptyResponseException, BadRequestException, UnAuthorisedException
from geocoding_api_exceptions import TimeoutException, InternalServerException, RateLimitExceededException, RequestTooLongException

class Utility():
    input_validation_error_message = "input address is not valid, please enter valid address"
    
    """ 
    validate the input address for special chars input
    """
    def validate_input_string(self, address):
        regex = re.compile(r'[@_!#$%^&*()<>?/\|}{~:+_=~`^\\-\\";.]') 
        if (regex.search(address) != None): 
            raise( InvalidAddressException( self.input_validation_error_message ) )    

    """ 
    Check the status code of api response and raise the exception according to that 
    """
    def check_status_code(self, status):
        error_msg = "Status code {} : ".format(status['code'])
        if status['code'] == 400:
            error_msg += "Invalid input address\nPlease enter valid address"
            raise BadRequestException(error_msg)

        elif status['code'] == 403:
            error_msg += "You are not authorised for this request, missing, invalid, or unknown API key"
            raise UnAuthorisedException(error_msg)

        elif status['code'] == 402:
            error_msg += "Your rate limit for api key has exceeded"
            raise RateLimitExceededException(error_msg)
        
        elif status['code'] == 408:
            error_msg += "Time out while getting longitude and latitude for input address, you can try again"
            raise TimeoutException(error_msg)
        
        elif status['code'] == 410:
            error_msg += "Your input address is too long."
            raise RequestTooLongException(error_msg)
        elif status['code'] == 510:
            error_msg += "Internal server error, you can try again"
            raise InternalServerException(error_msg)
    
